from snakemake.utils import validate
import json
print(json.dumps(config, indent=4))
validate(config, "config.schema.json")
print(json.dumps(config, indent=4))


name = config["name"]

rule doall:
        input:
                f"{name}_anchorwave.f.maf"

rule run_gff2sq:
        input:
                config['ref'],
                config['gff3']
        output:
                cds_fasta = f"{name}_cds.fasta"
        conda: 
                "env.yml"
        shell:
                """
                anchorwave gff2seq -i {input[1]} -r {input[0]} -o {output}
                """

rule run_minimap:
        input:
                rules.run_gff2sq.output[0],
                config['ref'],
                config['query']
        output:
                ref_sam = f"{name}_ref_cds.sam",
                query_sam = f"{name}_query_cds.sam"
        conda: 
                "env.yml"
        shell:
                """
                minimap2 -x splice -t 10 -k 12 -a -p 0.4 -N 20 {input[1]} {input[0]} > {output[0]}
                minimap2 -x splice -t 10 -k 12 -a -p 0.4 -N 20 {input[2]} {input[0]} > {output[1]}
                """

rule run_genoAli:
        input:
                config['ref'],
                config['gff3'], 
                rules.run_gff2sq.output[0],
                rules.run_minimap.output[0],
                rules.run_minimap.output[1],
                config['query']
        output:
                anchors = f"{name}_anchors",
                maf = f"{name}_anchorwave.maf",
                anchors_maf = f"{name}_anchorwave.f.maf"
        conda: 
                "env.yml"
        shell: 
                """
                anchorwave proali -t 4 -i {input[1]} -as {input[2]} -r {input[0]} -a {input[4]} -ar {input[3]} -s {input[5]} -n {output[0]} -o {output[1]} -f {output[2]} -R 1 -Q 1
                """       

 #anchorwave genoAli -t 3 -i {input[1]} -as {input[2]} -r {input[0]} -a {input[4]} -ar {input[3]} -s {input[5]} -n {output[0]} -o {output[1]} -f {output[2]} -IV